#!/usr/bin/env python3

__version__ = '1.0.0'

import requests
import re
import json
import argparse


parser = argparse.ArgumentParser(description='Creates json with hpo diseases and identfiers, example: {"Cutis Laxa, Neonatal, With Marfanoid Phenotype": "OMIM:614100"}')
parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
parser.add_argument("--output_name", "-o", help="path to the output file")
args = parser.parse_args()

url="http://compbio.charite.de/jenkins/job/hpo.annotations.current/lastSuccessfulBuild/artifact/current/phenotype.hpoa"

#TEST URL
#url="https://gitlab.com/intelliseq/workflows/-/raw/hpo-diseases/src/test/resources/data/txt/hpo-diseases.txt"


def dict_to_json(dictionary: dict, json_name: str):
    with open(json_name, 'w') as json_simple:
        json.dump(dictionary, json_simple, indent=2)

def get_dict(url):

    response = requests.get(url)
    hpo = response.text

    result = {}
    for i in hpo.split("\n")[:-1]:
        if i[0] != "#":
            line = i.split("\t")
            result[line[1]] = line[0]
    return result


if __name__ == '__main__':
    result = get_dict(url)
    dict_to_json(result, args.output_name)

#!/usr/bin/env bash

DATE=$1
SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/dataindex/' )"

OUTPUT="$PROJECT_DIR/hpo-diseases/releases/$DATE"
DOCKER="intelliseqngs/ubuntu-minimal-20.04:3.0.3"

mkdir -p $OUTPUT
echo "Creating directory..."
mkdir -p "$OUTPUT"

echo "Running..."
docker run --rm -it -v ${PROJECT_DIR}:/workflows -v $OUTPUT:/tmp -w /workflows $DOCKER python3 hpo-diseases/scripts/create-hpo-diseases-json.py -o /tmp/hpo-diseases.json

# dataindex
## table of contents
------------------------
- [Few words about dataindex repository](#few-words-about-dataindex-repository)
  * [In general](#in-general)
  * [Version controll](#version-controll)
- [List of resources](#list-of-resources)
  * [ensembl genes](#ensembl-genes)
    + [Description:](#description-)
    + [In tasks and modules](#in-tasks-and-modules)
    + [How to create](#how-to-create)
    + [Content of directory](#content-of-directory)
  * [ensembl species](#ensembl-species)
    + [Description](#description-)
    + [How to create](#how-to-create)
    + [Content of directory](#content-of-directory)  
  * [hpo-terms](#hpo-terms)
    + [Description:](#description--1)
    + [How to create](#how-to-create-1)
    + [Hpo term json content](#hpo-term-json-content)
  * [hpo-diseases](#hpo-diseases)
    + [Description:](#description--2)
    + [How to create](#how-to-create-2)
    + [Hpo diseases json content](#hpo-diseases-json-content)
    + 
<small><i><a href='http://ecotrust-canada.github.io/markdown-toc/'>Table of contents generated with markdown-toc</a></i></small>

## Few words about dataindex repository
---------------------------------------
### In general
Resources are needed in interface in workflows with "selectable" in meta. Files are too large to store them in "workflows" repository and for this reason are stored here in separated repository. Previously this resources were stored on the server (anakin). 

### Version control
Version control depends on the resource (date, number, semantic - depends on the source database).
Each relase commit is tagged: **resource-name@version**. For example **ensembl-genes@101**.
## List of resources
-----------------------------------
### ensembl genes
-------------------------
**Last update date:** (13-05-21)

**Update requirements:** <br />
This needs to be updated whenever the new version of ensembl database is released. 
To do this you need to modify ensembl-versions.json (add new version's name as a key and it's direct link as a value).
The file ensembl-versions.json can be found at: ```https://gitlab.com/intelliseq/dataindex/-/tree/dev/ensembl-genes/scripts/ensembl-versions.json```


**History of versions and updates:** <br />
**version** 104, **date** 13-05-2021 (limited column width issue, 103 and 104 update, test for single chromosome) <br />
https://gitlab.com/intelliseq/dataindex/-/raw/ensembl-genes@104/ensembl-genes/releases/104/ensembl-genes.json <br />
**version** 101, **date** 06-10-2020, (docker with python script and test version 1.0.0) <br />
https://gitlab.com/intelliseq/dataindex/-/raw/ensembl-genes@101/ensembl-genes/releases/101/ensembl-genes.json <br />
**version** 100, **date** 30-11-2020, (docker with python script and test version 1.0.0) <br />
https://gitlab.com/intelliseq/dataindex/-/raw/ensembl-genes@100/ensembl-genes/releases/100/ensembl-genes.json <br />

#### Description:
Task created to get genes from ensembl database (http://ensembl.org dataset hsapiens_gene_ensembl) and their synonyms in .json dictionary-like format.

#### How to create
Using your console you need to get into scripts catalog.
In this case it is ```~/dataindex/ensembl-genes/scripts``` in downloaded https://gitlab.com/intelliseq/dataindex/-/tree/dev repository.

Define version:
```
VERSION=104
```

The chosen version must be specified by the user.
To update it you need to check release date of new version in the Internet.
When you find it you must edit ```path/to/repo/ensembl-genes/scripts/ensembl-versions.json``` file.
This file contains version number with its link to ensembl website in a key-value .json format. For example:
```
{
  "latest": "http://ensembl.org",
  "104": "http://may2021.archive.ensembl.org/",
  "103": "http://feb2021.archive.ensembl.org/",
  "102": "http://nov2020.archive.ensembl.org/",
  "101": "http://aug2020.archive.ensembl.org/",
  "100": "http://apr2020.archive.ensembl.org/",
  "99": "http://jan2020.archive.ensembl.org/",
  "98": "http://sep2019.archive.ensembl.org/"
}
```
So for example if version 103 was released in february 2021 
it's archive link contains combination of three first letters 
of this month and a year: feb2021 (http://feb2021.archive.ensembl.org/)

After setting version you can run script locally with run.sh:
```
bash run.sh $VERSION
```

Or with following command:
```
python3 gene-name-synonym-to-json.py \
    --input-version '$VERSION' \
    --input-versions-json 'path/to/versions/json' \
    --output-dir 'path/to/output/directory'
```

To test script on a single chromosome in test.sh:
```
bash test.sh $VERSION
```


File contents
Gene name to find synonyms, Gene Synonyms separated by commas
Example format:
```
{
  "5S_rRNA": "",
  "5_8S_rRNA": "",
  "7SK": "",
  "A1BG": "",
  "A1BG-AS1": "Synonyms: A1BG-AS, A1BGAS, FLJ23569, NCRNA00181",
  "A1CF": "Synonyms: ACF, ACF64, ACF65, APOBEC1CF, ASP",
  "A2M": "Synonyms: CPAMD5, FWP007, S863-7",
  "A2M-AS1": "",
  "A2ML1": "Synonyms: CPAMD9, FLJ25179, p170"
}
```
-----------------------------------
### ensembl species
-------------------------
**Last update date:** (24-06-21)

**Update requirements:** <br />
This needs to be updated whenever the new version of ensembl database is released. 


**History of versions and updates:** <br />
**version** 104, **date** 24-06-2021 <br />
https://gitlab.com/intelliseq/dataindex/-/raw/ensembl-species@104/ensembl-species/releases/104/ensembl-species.json <br />

#### Description:
Task created to get species from ensembl database (http://ensembl.org dataset).

#### How to create
Using your console you need to get into scripts catalog.
In this case it is ```~/dataindex/ensembl-species/scripts``` in downloaded https://gitlab.com/intelliseq/dataindex/-/tree/dev repository.

Define version:
```
VERSION=104
```

After setting version you can run script locally with run.sh:
```
bash run.sh $VERSION
```

Or with following command:
```
python3 ensembl_species_to_json.py \
    --input-version '$VERSION' \
    --output-dir 'path/to/output/directory'
```

**File contents**

Species name as key and assembly as value.
Example format:
```
{
  "Spiny chromis": "ASM210954v1",
  "Eurasian sparrowhawk": "Accipiter_nisus_ver1.0",
  "Giant panda": "ASM200744v2",
  "Yellow-billed parrot": "ASM394721v1",
  "Midas cichlid": "Midas_v5",
  "Clown anemonefish": "AmpOce1.0",
  "Orange clownfish": "Nemo_v1"
}  
```

### hpo-terms
-------------------------

**Last update date:** 11-08-20

**Update requirements:** every release

**History of versions and updates:** (examples)

 **version** 11-08-20
 https://gitlab.com/intelliseq/dataindex/-/raw/hpo-terms@11-08-2020/hpo-terms/releases/11-08-2020/hpo-terms.json

#### Description:
This is json file with HPO terms and diseases from HPO database (Human Phenotype Onthology). https://hpo.jax.org/app/
This json is needed in intelliseqflow interface to choose diseases and hpo therms by user.
This resource is created based on database to out purposes.

#### How to create
```
RELEASE_DATE="11-08-2020"
bash ~/dataindex/hpo-terms/scripts/run.sh $RELEASE_DATE

```

#### Hpo term json content

```
{
"HP:3000072": "Abnormal levator palpebrae superioris morphology",
"HP:3000073": "Abnormality of levator veli palatini muscle",
"HP:3000074": "Abnormal lingual artery morphology"
}
```

### hpo-diseases
---------------------------------------------------

**Last update date:** 11-08-2020

**Update requirements:** every release, the same as above hpo-terms

**History of versions and updates:** (examples)

 **version** 11-08-20
 https://gitlab.com/intelliseq/dataindex/-/raw/hpo-diseases@11-08-2020/hpo-diseases/releases/11-08-2020/hpo-diseases.json

#### Description:
This is json file with HPO diseases and DB id from HPO database (Human Phenotype Onthology). https://hpo.jax.org/app/
This json is needed in intelliseqflow interface to choose diseases and hpo therms by user. This is the same source and release date as "hpo-terms", but there is seperate instruction how to create and separate scripts, because data derives from different files, with different data structure. Release data is in the middle bottom of the HPO website.

This resource is created based on database to out purposes.

#### How to create
```
RELEASE_DATE="11-08-20"
bash ~/dataindex/hpo-diseases/scripts/run.sh $RELEASE_DATE

```
#### Hpo diseases json content

```
{
"Familial cerebral saccular aneurysm": "ORPHA:231160",
"Temtamy syndrome": "ORPHA:1777"
}
```

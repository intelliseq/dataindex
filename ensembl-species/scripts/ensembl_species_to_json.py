#!/usr/bin/python3

import argparse
from pathlib import Path
import pandas as pd


__version__ = '0.0.1'


def read_tsv_to_df(tsv_path: str):
    return pd.read_csv(tsv_path, sep="\t", index_col=False)


def dataframe_to_json(version: str, output_dir: str):
    # creating a directory for saving the json file
    version_folder = f'{output_dir}/{version}'
    Path(version_folder).mkdir(parents=True, exist_ok=True)

    # read tsv from http
    ensembl_species = read_tsv_to_df(f"http://ftp.ensembl.org/pub/release-{version}/species_EnsemblVertebrates.txt")
    # select columns (name and assembly)
    ensembl_species = ensembl_species[["#name", "assembly"]]
    # create json file from df (#name as key and assembly as value)
    ensembl_species.set_index('#name')['assembly'].to_json(f'{version_folder}/ensembl-species.json')


if __name__ == "__main__":

    parser = argparse.ArgumentParser(description='Generate json file from ensembl species table '
                                                 '(name as key and assembly as value')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--input-version', '-i', metavar='input_version', type=str, required=True,
                        help='Version of Ensembl')
    parser.add_argument('--output-dir', '-o', metavar='output_dir', type=str, required=True,
                        help='Directory where the version catalog should be done')
    args = parser.parse_args()
    dataframe_to_json(version=args.input_version, output_dir=args.output_dir)

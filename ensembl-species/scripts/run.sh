
docker run --rm -it -v $PWD/../releases/:/outputs \
  -v $PWD/:/intelliseqtools \
intelliseqngs/ensembl-species-to-json:1.0.0 \
python3 /intelliseqtools/ensembl_species_to_json.py \
--input-version $1 \
--output-dir '/outputs'


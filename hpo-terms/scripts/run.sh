#!/usr/bin/env bash

DATE=$1

SOURCE="${BASH_SOURCE[0]}"
PROJECT_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd | grep -oh '.*/dataindex/' )"

OUTPUT="$PROJECT_DIR/hpo-terms/releases/$DATE"
DOCKER="intelliseqngs/ubuntu-minimal-20.04:3.0.3"

mkdir -p $OUTPUT

docker run --rm -it -v ${PROJECT_DIR}:/dataindex -v $OUTPUT:/tmp -w /dataindex $DOCKER python3 hpo-terms/scripts/create-hpo-terms-json.py -o /tmp/hpo-terms.json

echo "Output file:"
echo $OUTPUT/hpo-terms.json
cat $OUTPUT/hpo-terms.json | head



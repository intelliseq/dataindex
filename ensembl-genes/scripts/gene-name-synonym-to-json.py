#!/usr/bin/python3
from pybiomart import Dataset
import json
import argparse
from requests.exceptions import HTTPError, ConnectionError
import time
from pathlib import Path
import pandas as pd


__version__ = "0.0.1"
AUTHOR = "gitlab.com/olaf.tomaszewski"

pd.set_option('display.max_colwidth', 1000)


def attempt_to_get_database_query(versions_json, version, chromosome_names):
    biomart_available = False
    num_of_attempts = 0
    max_n_of_attempts = 2
    while not biomart_available:
        try:
            return get_database_query(versions_json, version, chromosome_names)
            biomart_available = True
        except (HTTPError, ConnectionError):
            if num_of_attempts >= max_n_of_attempts:
                raise
            time.sleep(2 ** num_of_attempts)
        num_of_attempts += 1


def generate_chromosome_names(input_optional_single_chromosome):
    if input_optional_single_chromosome:
        return list(input_optional_single_chromosome)
    chromosome_numbers = [str(x) for x in range(1,23)]
    return chromosome_numbers + ['X', 'Y', 'MT']


def get_database_query(versions_json, version, chromosome_names):
    with open(versions_json) as versions_file:
        versions_dict = json.load(versions_file)

    dataset = Dataset(name='hsapiens_gene_ensembl',
                      host=f'{versions_dict[version]}')

    queried = dataset.query(attributes=['external_gene_name', 'external_synonym'],
                            filters={'chromosome_name': chromosome_names})
    return queried


def rename_na_and_aggregate(gene_synonym):
    gene_synonym = gene_synonym.fillna('')

    aggregated = gene_synonym.groupby('Gene name', as_index=True).aggregate({
        'Gene name': lambda x: x.iloc[0],
        'Gene Synonym': lambda x: ', '.join(x)
    })

    return aggregated


def get_gene_synonym_dict(indexes):
    gene_synonym_dict = dict()

    for index in indexes:
        gene_mask = aggregated['Gene name'] == index
        gene_synonym_dict[index] = aggregated[gene_mask]['Gene Synonym'].to_string(header=False,
                                                                                   index=False).lstrip()
        if gene_synonym_dict[index]:
            gene_synonym_dict[index] = "Synonyms: " + gene_synonym_dict[index]

    return gene_synonym_dict


def create_version_catalog_and_save_json(gene_synonym_dict, version, output_dir):
    version_folder = f'{output_dir}/{version}'
    Path(version_folder).mkdir(parents=True, exist_ok=True)

    with open(f'{version_folder}/ensembl-genes.json', 'w') as json_file:
        json.dump(gene_synonym_dict, json_file, indent=2)


def get_index_values_as_list(aggregated):
    return aggregated.index.values.tolist()


def args_parser_init():
    parser = argparse.ArgumentParser(description='Generates .json file with ')
    parser.add_argument('--input-optional-single-chromosome', '-t', metavar='input_optional_single_chromosome',
                        default="", type=str, required=False,
                        help='Single chromosome used for test purposes')
    parser.add_argument('--input-version', '-i', metavar='input_version', type=str, required=True,
                        help='Version of dataset')
    parser.add_argument('--input-versions-json', '-j', metavar='input_versions_json', type=str, required=True,
                        help='Release versions in key-value format (version-link)')
    parser.add_argument('--output-dir', '-o', metavar='output_dir', type=str, required=True,
                        help='Directory where the version catalog should be done')
    parser.add_argument('-v', '--version', action='version', version='%(prog)s {}'.format(__version__))
    parser.add_argument('--max_attempts', type=int, default=8,
                        help='Max number of attempts when retrieving data from biomart.')
    args = parser.parse_args()
    return args


if __name__ == "__main__":
    args = args_parser_init()
    chromosome_names = generate_chromosome_names(args.input_optional_single_chromosome)

    gene_synonym = attempt_to_get_database_query(args.input_versions_json, args.input_version, chromosome_names)
    aggregated = rename_na_and_aggregate(gene_synonym)

    indexes = get_index_values_as_list(aggregated)
    gene_synonym_dict = get_gene_synonym_dict(indexes)

    create_version_catalog_and_save_json(gene_synonym_dict, args.input_version, args.output_dir)

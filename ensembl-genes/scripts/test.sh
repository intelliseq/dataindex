mkdir -p /tmp/test/ensembl-genes

docker run --rm -it -v /tmp/test/ensembl-genes/:/outputs \
  -v $PWD/:/intelliseqtools \
intelliseqngs/gene-name-synonym-to-json:1.0.3 \
python3 /intelliseqtools/gene-name-synonym-to-json.py \
--input-optional-single-chromosome '1' \
--input-version '104' \
--input-versions-json '/intelliseqtools/ensembl-versions.json' \
--output-dir '/outputs'


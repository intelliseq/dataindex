docker run --rm -it -v $PWD/../releases/:/outputs \
  -v $PWD/:/intelliseqtools \
intelliseqngs/gene-name-synonym-to-json:1.0.3 \
python3 /intelliseqtools/gene-name-synonym-to-json.py \
--input-version '104' \
--input-versions-json '/intelliseqtools/ensembl-versions.json' \
--output-dir '/outputs'

